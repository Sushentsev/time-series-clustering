import numpy as np
import matplotlib.pyplot as plt


def plot_clusters(time_series: np.ndarray, clusters: np.ndarray, metric_name: str):
    labels = np.unique(clusters)
    fig, axs = plt.subplots(len(labels), figsize=(15, 20))
    fig.suptitle(f"Кластеризация с метрикой {metric_name}")

    for label, ax in zip(labels, axs):
        ax.set_title(f"Кластер {label}")
        ax.set_xlabel("Минута")
        ax.set_ylabel("Цена")

        samples = time_series[clusters == label]
        for sample in samples:
            ax.plot(range(60), sample)
