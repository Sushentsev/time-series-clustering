import sqlite3
import pandas as pd
from os.path import join


def create_connection(db_file: str):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)
    return conn


def select_data(conn, table: str):
    return pd.read_sql(f"SELECT * FROM {table}", conn)


if __name__ == "__main__":
    conn = create_connection("./data/trade_info.sqlite3")

    for table_name in ["chart_data", "trading_session"]:
        select_data(conn, table_name).to_csv(join("./data", table_name + ".csv"), index=False)
