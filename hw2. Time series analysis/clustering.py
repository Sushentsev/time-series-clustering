from typing import Tuple

import numpy as np
from dtaidistance import dtw
from scipy.spatial.distance import cosine
from sklearn.cluster import DBSCAN


def cosine_distance(x: np.ndarray, y: np.ndarray) -> float:
    return cosine(x, y)


def mae_distance(x: np.ndarray, y: np.ndarray) -> float:
    return np.sum(np.abs(x - y))


def dtw_distance(x: np.ndarray, y: np.ndarray) -> float:
    return dtw.distance(x, y)


def cluster(X: np.ndarray, eps: float = 0.5, min_samples: int = 5, metric: str = "cos") -> np.ndarray:
    metrics = {"cos": cosine_distance, "mae": mae_distance, "dtw": dtw_distance}
    return DBSCAN(eps=eps, min_samples=min_samples, metric=metrics[metric]).fit_predict(X)
