import datetime
import math
from typing import List

import pandas as pd


def make_summary(trading_session, chart_data):
    summary = chart_data.join(trading_session.set_index("id"), on="session_id", how="inner")
    summary["datetime"] = pd.to_datetime(summary.date + " " + summary.time)
    summary = summary.drop(columns=["time", "date", "trading_type"], axis=1)
    summary = summary.sort_values(by="datetime")
    return summary


def remove_long_sessions(summary):
    session_info = summary.groupby(by=["session_id"]).datetime.agg(["min", "max"])
    session_info["duration"] = session_info["max"] - session_info["min"]
    wrong_sessions = session_info[session_info.duration >= datetime.timedelta(hours=1)].index.values
    summary = summary[~summary.id.isin(wrong_sessions)]
    return summary


def remove_outliers(summary):
    return summary[((summary.platform_id == 1) & (summary.datetime.dt.hour >= 11) & (summary.datetime.dt.hour < 12)) |
                   ((summary.platform_id == 2) & (summary.datetime.dt.hour >= 12) & (summary.datetime.dt.hour < 13))]


def make_time_series(df) -> List[float]:
    time_series = [None] * 60
    time_series_df = df.set_index("datetime").price.resample("1T").mean()

    for time, price in time_series_df.items():
        time_series[time.minute] = price

    default_value = time_series_df[0]

    for i, value in enumerate(time_series):
        if value is None or math.isnan(value):
            time_series[i] = default_value
        else:
            default_value = value

    return time_series
